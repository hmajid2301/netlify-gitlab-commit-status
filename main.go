package webhook

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strings"

	"github.com/GoogleCloudPlatform/functions-framework-go/functions"
	jwt "github.com/golang-jwt/jwt/v4"
	gitlab "github.com/xanzy/go-gitlab"
)

func init() {
	functions.HTTP("webhook", webhook)
}

type NetlifyWebhookPayload struct {
	CommitRef string `json:"commit_ref"`
	State     string `json:"state"`
	CommitURL string `json:"commit_url"`
}

func webhook(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method Not Allowed"))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if !signed(r, body) {
		http.Error(w, "invalid signature", http.StatusForbidden)
		return
	}

	var payload NetlifyWebhookPayload
	err = json.NewDecoder(bytes.NewReader(body)).Decode(&payload)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var rx = regexp.MustCompile(`^https:\/\/gitlab.com\/(.*?)\/(.*?)\/`)
	matches := rx.FindStringSubmatch(payload.CommitURL)
	if len(matches) < 3 {
		fmt.Println(fmt.Printf("Invalid commit URL %s", payload.CommitURL))
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Skipped"))
		return
	}

	commitURL := payload.CommitURL
	repoURL := strings.Split(commitURL, "/commit")[0]
	namespace := strings.Replace(repoURL, "https://gitlab.com/", "", 1)
	lastInd := strings.LastIndex(namespace, "/")
	owner := namespace[:lastInd]
	repo := namespace[lastInd+1:]


	gitlabToken := os.Getenv("GITLAB_TOKEN")
	client, err := gitlab.NewClient(gitlabToken)
	if err != nil {
		fmt.Println("failed to create GitLab client: %w", err)
		http.Error(w, "failed to create GitLab client", http.StatusInternalServerError)
		return
	}

	projectID, err := getProjectId(client, owner, repo)
	if err != nil {
		fmt.Println("failed to get project ID: %w", err)
		http.Error(w, "failed to get project ID", http.StatusInternalServerError)
		return
	}
	var state gitlab.BuildStateValue

	stateMap := map[string]gitlab.BuildStateValue{
		"building": gitlab.Pending,
		"ready":    gitlab.Success,
		"error":    gitlab.Failed,
	}
	state, ok := stateMap[payload.State]
	if !ok {
		fmt.Println(fmt.Printf("state not in expected state map %s", payload.State))
		http.Error(w, "failed to get state", http.StatusInternalServerError)
		return
	}

	_, _, err = client.Commits.SetCommitStatus(projectID, payload.CommitRef, &gitlab.SetCommitStatusOptions{
		State: state,
		Name:  gitlab.String("Deploy to Netlify"),
	})
	if err != nil {
		fmt.Println("failed to create commit status: %w", err)
		http.Error(w, "failed to create commit status", http.StatusInternalServerError)
		return
	}
	fmt.Println(fmt.Printf("created commit status for project %s", payload.CommitURL))
	w.Write([]byte("created commit status"))
}

func signed(r *http.Request, body []byte) bool {
	signature := r.Header.Get("X-Webhook-Signature")
	if signature == "" {
		return false
	}

	options := jwt.MapClaims{
		"iss":       "netlify",
		"algorithm": "HS256",
	}

	jwsSecret := os.Getenv("JWS_SECRET")
	decoded, err := jwt.ParseWithClaims(signature, options, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(jwsSecret), nil
	})
	if err != nil {
		return false
	}

	payload, ok := decoded.Claims.(jwt.MapClaims)
	if !ok {
		return false
	}
	sha256Field, ok := payload["sha256"]
	if !ok {
		return false
	}
	sha256String, ok := sha256Field.(string)
	if !ok {
		return false
	}

	bodySha := sha256.Sum256(body)
	expectedSignature := hex.EncodeToString(bodySha[:])

	return sha256String == expectedSignature
}

func getProjectId(client *gitlab.Client, owner, repo string) (int, error) {
	var projectID int
	owned := true

	search := fmt.Sprintf("%s", repo)
	opts := &gitlab.ListProjectsOptions{
		Owned:  &owned,
		Search: &search,
	}
	projects, _, err := client.Projects.ListProjects(opts)
	if err != nil {
		return projectID, err
	}

	for _, project := range projects {
		if project.PathWithNamespace == owner+"/"+repo {
			projectID = project.ID
			break
		}
	}

	if projectID == 0 {
		return projectID, fmt.Errorf("project not found")
	}

	return projectID, nil
}
