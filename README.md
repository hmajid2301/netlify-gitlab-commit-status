# Netlify Commit Status

At the moment Netlify has no way to update the commit status of non MR commits.
So I created this simple handler function which expects a Netlify deployment webhook.

You can see an example of what it looks like:

![CI pipeline](images/ci_pipeline.png)

You can create a Webhook go to your website in Netlify:

- Go to `Site settings`
- Then `Build & deploy`
- Go to `Deploy notifications`
- Add a new notification pointing to where you have deployed this app.
  - Keep note of JWS secret token, this is used to check the request actually come from

![Netlify Webhook](images/netlify_webhook.png)

## Env Variables

We need to two environment variables:

- `JWS_SECRET`: Set in the outgoing hook (`ThisIsASecret` above)
- `GITLAB_TOKEN`: From an access token on GitLab
